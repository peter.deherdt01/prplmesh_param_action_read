# check pre-requisites
EXECUTABLES = install strip ln tar $(CC) $(AR) fakeroot mkdir git echo cat
K := $(foreach exec,$(EXECUTABLES),\
        $(if $(shell which $(exec)),some string,$(error "No $(exec) in PATH")))

# tools used
INSTALL = $(shell which install)
STRIP = $(shell which strip)
LN = $(shell which ln)
TAR = $(shell which tar)
FAKEROOT = $(shell which fakeroot)
MKDIR = $(shell which mkdir)
GIT = $(shell which git)
ECHO = $(shell which echo)
CAT = $(shell which cat)

# timestamp
NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# the current version
RAW_VERSION ?= $(if $(shell git describe --tags 2> /dev/null),$(shell git describe --tags),v0.0.0)
VERSION = $(subst v,,$(strip $(RAW_VERSION)))
VERSION_PARTS = $(subst ., ,$(VERSION))
VMAJOR = $(word 1,$(VERSION_PARTS))
VMINOR = $(word 2,$(VERSION_PARTS))
VBUILD_FULL = $(word 3,$(VERSION_PARTS))
VBUILD = $(word 1,$(subst -, ,$(VBUILD_FULL)))

# the processor architecture 
MACHINE = $(shell $(CC) -dumpmachine)
CC_PARTS = $(subst -, ,$(CC))
CC_NAME = $(word $(words $(CC_PARTS)),$(CC_PARTS))

# Source dir
SRCDIR = ./src

# Include dir
INCDIR_PRIV = ./include_priv
INCDIRS = $(INCDIR_PRIV)

# Output dir
OUTPUTDIR = ./output/$(MACHINE)
OBJDIR = $(OUTPUTDIR)/object

# Target
TARGET_NAME = app
TARGET = $(OBJDIR)/$(TARGET_NAME)

# Sources and objects
SOURCES = $(wildcard $(SRCDIR)/*.c)
OBJECTS = $(addprefix $(OBJDIR)/,$(notdir $(SOURCES:.c=.o)))

# install directories
PREFIX = /usr
DEST ?= $(OUTPUTDIR)/build
INSTALL_INC_DIR = include/$(TARGET_NAME)
INSTALL_LIB_DIR ?= lib/$(MACHINE)
INSTALL_BIN_DIR = /bin
INSTALL_CFG_DIR = /etc/amx
