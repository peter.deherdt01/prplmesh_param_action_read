/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <signal.h>

#include <event2/event.h>

#include "app.h"

typedef struct _app_el {
    struct event_base* base;
    struct event* sig_int;
    struct event* sig_term;
    struct event* sig_alarm;
    struct event* amxp_sig;
    struct event* bus;
} app_el_t;

static app_el_t el = {
    .base = NULL,
    .sig_int = NULL,
    .sig_term = NULL,
    .sig_alarm = NULL,
    .amxp_sig = NULL,
    .bus = NULL
};

// system signals sigint and sigterm
static void app_el_signal_cb(UNUSED evutil_socket_t fd,
                             UNUSED short event,
                             UNUSED void* arg) {
    // are stopping the eventloop and will cause the application to exit
    app_el_stop();
}

// system signal sigalarm
static void app_el_timers(UNUSED evutil_socket_t fd,
                          UNUSED short event,
                          UNUSED void* arg) {
    // will calculate the remaining time of the active timers
    amxp_timers_calculate();
    // and checks if a timer has expired,
    // if so this will call the timer cb function
    // it will restart timers if an interval was set
    amxp_timers_check();

}

// bus file descriptors will be handled here
static void app_el_bus_cb(UNUSED evutil_socket_t fd,
                          UNUSED short flags,
                          void* arg) {
    amxb_bus_ctx_t* bus_ctx = (amxb_bus_ctx_t*) arg;
    // Call the bus specific read function using the Bus Agnostic API.
    // The bus library will dispatch to the correct callback functions
    // The bus specific back-end will do translation were needed.
    amxb_read(bus_ctx);
}

// The amxp signal/slot file descriptor will be handled here
static void app_el_events_cb(UNUSED evutil_socket_t fd,
                             UNUSED short flags,
                             UNUSED void* arg) {
    // Dispatch the available signals (events) and call the correct
    // callback functions (slots)
    amxp_signal_read();
}

int app_el_create(amxb_bus_ctx_t* bus_ctx) {
    // Fetch the bus file descriptor
    int fd = amxb_get_fd(bus_ctx);
    if(fd < 0) {
        printf("Invalid bus file descriptor [%d]\n", fd);
        return -1;
    }

    el.base = event_base_new();
    if(el.base == NULL) {
        printf("Creation of event base failed\n");
        return -1;
    }

    //=========================================================================
    // Add the system signals to the eventloop and set the correct callback
    // functions
    //=========================================================================
    el.sig_int = evsignal_new(el.base,
                              SIGINT,
                              app_el_signal_cb,
                              NULL);
    event_add(el.sig_int, NULL);
    el.sig_term = evsignal_new(el.base,
                               SIGTERM,
                               app_el_signal_cb,
                               NULL);
    event_add(el.sig_term, NULL);
    el.sig_alarm = evsignal_new(el.base,
                                SIGALRM,
                                app_el_timers,
                                NULL);
    event_add(el.sig_alarm, NULL);
    //=========================================================================

    //=========================================================================
    // Add the events (amxp signals) file descriptor to the eventloop and set
    // the correct callback functions
    // This file descriptor makes the amxp signal/slot mechanism work.
    // The data model events are all based on the amxp signal/slot
    // implementation
    //=========================================================================
    el.amxp_sig = event_new(el.base,
                            amxp_signal_fd(),
                            EV_READ | EV_PERSIST,
                            app_el_events_cb,
                            NULL);
    event_add(el.amxp_sig, NULL);
    //=========================================================================

    //=========================================================================
    // Add the bus file descriptor to the eventloop and set the correct
    // callback function
    //=========================================================================
    el.bus = event_new(el.base,
                       fd,
                       EV_READ | EV_PERSIST,
                       app_el_bus_cb,
                       bus_ctx);
    event_add(el.bus, NULL);
    //=========================================================================

    return 0;
}

int app_el_start(void) {
    if(el.base != NULL) {
        return event_base_dispatch(el.base);
    } else {
        return -1;
    }
}

int app_el_stop(void) {
    if(el.base != NULL) {
        return event_base_loopbreak(el.base);
    } else {
        return -1;
    }
}

int app_el_destroy(void) {
    if(el.sig_int != NULL) {
        event_del(el.sig_int);
    }
    if(el.sig_term != NULL) {
        event_del(el.sig_term);
    }
    if(el.sig_alarm != NULL) {
        event_del(el.sig_alarm);
    }
    if(el.amxp_sig != NULL) {
        event_del(el.amxp_sig);
    }
    if(el.bus != NULL) {
        event_del(el.bus);
    }
    if(el.base != NULL) {
        event_base_free(el.base);
        el.base = NULL;
    }

    free(el.sig_int);
    el.sig_int = NULL;
    free(el.sig_term);
    el.sig_term = NULL;
    free(el.sig_alarm);
    el.sig_alarm = NULL;
    free(el.amxp_sig);
    el.amxp_sig = NULL;
    free(el.bus);
    el.bus = NULL;

    return 0;
}
