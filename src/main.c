/****************************************************************************
**
** - DISCLAIMER OF WARRANTY -
**
** THIS FILE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
** EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
** PURPOSE.
**
** THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOURCE
** CODE IS WITH YOU. SHOULD THE SOURCE CODE PROVE DEFECTIVE, YOU
** ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
**
** - LIMITATION OF LIABILITY -
**
** IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
** WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES
** AND/OR DISTRIBUTES THE SOURCE CODE, BE LIABLE TO YOU FOR DAMAGES,
** INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
** ARISING OUT OF THE USE OR INABILITY TO USE THE SOURCE CODE
** (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED
** INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE
** OF THE SOURCE CODE TO OPERATE WITH ANY OTHER PROGRAM), EVEN IF SUCH
** HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
** DAMAGES.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <signal.h>

#include "app.h"

#define DEFAULT_ODL_PATH "/etc/amx/"

static char* get_odl_file_name(const char* app_name) {
    amxc_string_t odl_path;
    const char* prefix = getenv("AMXO_PREFIX_PATH");

    if(prefix == NULL) {
        prefix = "";
    }

    amxc_string_init(&odl_path, 0);
    //amxc_string_setf(&odl_path, "%s%s%s/%s.odl", prefix, DEFAULT_ODL_PATH, app_name, app_name);
    amxc_string_setf(&odl_path, "/etc/amx/app/app.odl");
    return amxc_string_take_buffer(&odl_path);
}

static void app_set_last_changed(void) {
    amxc_ts_t now;
    amxd_object_t* controller = amxd_dm_findf(app_get_dm(), "Controller.");

    amxc_ts_now(&now);
    amxd_object_set_value(uint64_t, controller, "LastChange", now.sec);
}

int main(int argc, char* argv[]) {
    int retval = 0;
    amxb_bus_ctx_t* bus_ctx = NULL;
    amxd_dm_t* dm = NULL;
    char* odl = get_odl_file_name(argv[0]);

    // initialize bus agnostic connection and data model
    retval = app_init(argv[1], odl);
    if(retval != 0) {
        goto exit;
    }

    bus_ctx = app_get_bus_ctx();
    dm = app_get_dm();

    app_set_last_changed();

    // create an eventloop (using libevent)
    if(app_el_create(bus_ctx) != 0) {
        retval = 5;
        goto exit;
    }

    printf("*************************************\n");
    printf("*            App started            *\n");
    printf("*************************************\n");

    // trigger "app:start" signal
    //    - automatic data model instance counters use this signal to get initialized
    amxp_sigmngr_trigger_signal(&dm->sigmngr, "app:start", NULL);
    // and then start the eventloop
    if(app_el_start() != 0) {
        retval = 6;
    }
    // trigger "app:stop" signal
    amxp_sigmngr_trigger_signal(&dm->sigmngr, "app:stop", NULL);

    printf("*************************************\n");
    printf("*            App stopped            *\n");
    printf("*************************************\n");

exit:
    free(odl);
    // clean-up the eventloop (libevent)
    app_el_destroy();
    // cleanup bus agnostic connection and data model
    app_cleanup();
    return retval;
}
